Установка
=========
```bash
sudo lsb_release -a
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 16.04.3 LTS
Release:        16.04
Codename:       xenial
```

oracle instantclient
--------------------
- Качаем: http://www.oracle.com/technetwork/database/features/instant-client/index.html

```
instantclient-basic-linux.x64-12.2.0.1.0.zip
instantclient-sdk-linux.x64-12.2.0.1.0.zip
instantclient-sqlplus-linux.x64-12.2.0.1.0.zip
```
- Распаковываем

```bash
mkdir -p /opt/oracle
sudo unzip instantclient-basic-linux.x64-12.2.0.1.0.zip -d /opt/oracle/
sudo unzip instantclient-sdk-linux.x64-12.2.0.1.0.zip -d /opt/oracle/
sudo unzip instantclient-sqlplus-linux.x64-12.2.0.1.0.zip -d /opt/oracle/
```
- Создаем ссылки

```bash
cd /opt/oracle/instantclient_12_2/
ls -s libclntsh.so.12.1 libclntsh.so
```
oci8
----
```bash
pecl install oci8
```
На вопрос где искать instantclient, указываем: `instantclient,/opt/oracle/instantclient_12`
pdo_oci
-------
```bash
wget http://de2.php.net/get/php-7.0.22.tar.gz/from/this/mirror
tar xfvz mirror
cd php-7.0.22/exp/pdo_oci
phpize
./configure --with-pdo-oci=instantclient,/opt/oracle/instantclient_12
make
```
Готовые бинарники
-----------------
[oci8](20151012/oci8.so)  
[pdo_oci](20151012/pdo_oci.so)
